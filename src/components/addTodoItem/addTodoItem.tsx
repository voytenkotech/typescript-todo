import React, { ChangeEvent} from 'react';

import './addTodoItem.css';

const AddTodoItem = ({addItem, parentChannel}: any) => {
    

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const newLabel: string = event.target.value
        parentChannel(newLabel)
      }
    return (
        <div className="input-group add-todo-item">
            <input
                type="text"
                className="input-group-text form-control"
                placeholder="Change resume"
                onChange={handleChange} 
                >    
            </input>
            <button
                type="button"
                onClick={addItem}
                className="btn btn-primary">
                    Add
            </button>
        </div>
    )
}

export default AddTodoItem;