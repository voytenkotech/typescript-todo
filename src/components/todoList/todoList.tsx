
import React from 'react';

import './todoList.css'

import { ITodoListItem } from '../../interfaces';

const TodoList: React.FC<
    {todos: ITodoListItem[], DeleteItem: any,
    onToggleDone: any, onToggleImportant: any}> = 
    ({todos, DeleteItem, onToggleDone, onToggleImportant}) => {
    const elements = todos.map((item: ITodoListItem) => {

        const { id, label, done, important } = item;

        let classNames = "list-group-item"
        if (done) {
            classNames += ' done'
        }
        if (important) {
            classNames += ' important'
        }
        return(
            <span className={classNames} key={id}>
                <span 
                    className="list-group-item-label"
                    onClick={() => onToggleDone(id)}>
                        {label}
                </span>
                <div>
                <button type="button"
                    className="btn btn-outline-danger btn-sm"
                    onClick={() => DeleteItem(id)}>
                    <i className="fa-solid fa-trash" />
                </button>
                <button type="button"
                    className="btn btn-outline-success btn-sm"
                    onClick={() => onToggleImportant(id)}>
                    <i className="fa-solid fa-exclamation" />
                </button>
                </div>
            </span>
        )
    })
    return (
        <div className="todoList">
            <div className="list-group">
                {elements}
            </div>
        </div>
    )
}

export default TodoList;