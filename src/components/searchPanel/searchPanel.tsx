import React, {useState, ChangeEvent} from 'react';

import './searchPanel.css';

const SearchPanel :React.FC<
    { filterItem: string ,FilterChange: any, onSearchChange: any}> = 
    ({filterItem, FilterChange, onSearchChange}) => {

    const buttonsInfo = [
        { name: 'all', label: 'All'},
        { name: 'active', label: 'Active'},
        { name: 'done', label: 'Done'}
    ]

    const buttons = buttonsInfo.map(({name, label}) => {
        const isActive = filterItem === name;
        const addClass = isActive ? 'btn-info' : 'btn-outline-secondary';

        return (
            <button type="button"
                    className={`btn ${addClass}`}
                    key={name}
                    onClick={() => FilterChange(name)}>
                {label}
            </button>
        )
    })

    const onHandleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const term = event.target.value
        onSearchChange(term)
    }
    return (
        <div className="input-group searchPanel">
            <input 
                type="text"
                className="form-control input-group-text"
                placeholder="Search"
                arira-label="Search"
                onInput={onHandleChange}/> 
            <div className='btn-group'>
                {buttons}
            </div>
        </div>
    )
}

export default SearchPanel;