import React, { useState} from 'react';

import './app.css';

import Header from '../header'
import SearchPanel from '../searchPanel';
import TodoList from '../todoList';
import { ITodoListItem } from '../../interfaces';
import AddTodoItem from '../addTodoItem';
import { isTemplateSpan } from 'typescript';

type Filter = "all" | "active" | "done";

const App = () => {

    const [msgFromChild, setMsgFromChild] = useState('');
    const getMsgFromChild = (msg: string) => setMsgFromChild(msg);
    const [filterItem , setFilterItem] = useState('all')
    const [state = [
        {label: 'Rewrite App on TypeScript', id: 1, done: true, important: false},
        {label: 'Learn JS-React-Redux courses', id: 2, done: false, important: true},
        {label: 'Watch youtube', id: 3, done: false, important: false}
    ], setState] = useState<ITodoListItem[] | undefined>()

    const [term, setTerm] = useState('')

    const createTodoItem = () => {
        return {
            label: msgFromChild,
            id: state.length +2,
            done: false,
            important: false,
        }
    }

    const AddItem = () => {
        const newItem:ITodoListItem = createTodoItem();
        const newArray= [
            newItem,
            ...state
        ]
        console.log(newArray)
        setState(newArray)

    }
    const DeleteItem = (id: number) => {
        const idx = state.findIndex((el) => el.id === id)
        const newArray = [
            ...state.slice(0, idx),
            ...state.slice(idx + 1)
        ]
        setState(newArray)
    }
    
    const toggleProperty = (id: number) => {
        const idx = state.findIndex((el) => el.id === id)
        const oldItem = state[idx]
        const newItem = {...oldItem, 'done' : !oldItem['done']}
        return [
            ...state.slice(0, idx), 
            newItem,
            ...state.slice(idx + 1)
        ];
    }
    const onToggleDone = ( id:number) => {
        setState(toggleProperty(id))
    }

    const togglePropertyImporant = (id: number) => {
        const idx = state.findIndex((el) => el.id === id)
        const oldItem = state[idx]
        const newItem = {...oldItem, 'important' : !oldItem['important']}
        return [
            ...state.slice(0, idx), 
            newItem,
            ...state.slice(idx + 1)
        ];
    }
    const onToggleImportant = (id: number) => {
        setState(togglePropertyImporant(id))
    }

    const filter = (state: any, filterItem: string) =>  {
        switch(filterItem) {
            case 'all': 
                return state
            case 'active':
                return state.filter((item: any) => !item.done);
            case 'done':
                return state.filter((item: any) => item.done);
            default:
                return state;
        }
    }

    const onFilterChange = (filterItem: Filter) => {
        setFilterItem(filterItem)
    }

    const search = (state: any, term: string) => {
        if (term.length === 0) {
            return state
        }
        return state.filter((item: any) => {
            return item.label
                .toLowerCase()
                .indexOf(term.toLowerCase()) > - 1
        })
    }

    const onSearchChange = (term: any) => {
        console.log(term, 'onSearch')
        setTerm(term)
    }

    const visible = filter(search(state, term), filterItem)
    const countDone = state.filter((item) => item.done).length;
    const todoCount = state.length - countDone

    return (
        <div className="App">
            <Header toDo={todoCount} done={countDone}/>
            <SearchPanel 
                filterItem={filterItem}
                FilterChange={onFilterChange}
                onSearchChange={onSearchChange}/>
            <AddTodoItem 
                parentChannel = {getMsgFromChild}
                addItem={AddItem}/>
            <TodoList
                onToggleImportant={onToggleImportant}
                DeleteItem={DeleteItem}
                onToggleDone={onToggleDone}
                todos={visible}/>
        </div>
    )
}

export default App;