import React from 'react';

import './header.css';

const Header : React.FC<
    {toDo: number, done: number}> = ({toDo, done}) => {
        
        return (
            <div className="header d-flex">
                <h1>TS-Todo</h1>
                <h3>{toDo} more to do, {done} done</h3>
            </div>
        )
    }

export default Header;