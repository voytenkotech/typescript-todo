export interface ITodoListItem {
    label: string,
    id: number,
    done: boolean,
    important: boolean
}

export interface ITodoList{
    state : ITodoListItem[]
}

export interface IFilter{
    name: 'string'
    label?: 'string'
}